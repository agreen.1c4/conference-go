from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import json
import requests


def get_image(city, state):
    headers = {
        "Authorization": PEXELS_API_KEY,
    }

    params = {
        "per_page": 1,
        "query": f"{city} {state}",
        "size": "original",
    }

    url = "https://api.pexels.com/v1/search"
    response = requests.get(url, headers=headers, params=params)
    content = json.loads(response.content)
    try:
        return {"image_url": content["photos"][0]["src"]["original"]}
    except (KeyError, IndexError):
        return {"image_url": None}


def get_weather_data(city, state):
    ISO = str(3166 - 2)

    params_geo = {
        "q": f"{city}, {state}, {ISO}",
        "appid": OPEN_WEATHER_API_KEY,
    }
    url_geo = "http://api.openweathermap.org/geo/1.0/direct"
    response_geo = requests.get(url_geo, params=params_geo)
    content_geo = json.loads(response_geo.content)
    try:
        lat = content_geo[0].get("lat")
        lon = content_geo[0].get("lon")
    except IndexError:
        return None

    params_weather = {
        "lat": lat,
        "lon": lon,
        "appid": OPEN_WEATHER_API_KEY,
        "units": "imperial",
    }
    url_weather = "https://api.openweathermap.org/data/2.5/weather"
    res_weather = requests.get(url_weather, params=params_weather)
    content_weather = json.loads(res_weather.content)
    try:
        return {
            "weather": {
                "description": content_weather["weather"][0]["description"],
                "temp": content_weather["main"]["temp"],
            }
        }
    except:
        return {
            "weather": None,
        }
